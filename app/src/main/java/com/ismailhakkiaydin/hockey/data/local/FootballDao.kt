package com.nzdinc.hockey.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.nzdinc.hockey.model.leaguetable.Standing
import com.nzdinc.hockey.model.topscorer.Topscorer

@Dao
interface hockeyDao {

    @Insert
    suspend fun insertAllTopscorer(vararg topscorer: Topscorer): List<Long>

    @Query("SELECT * FROM topscorer ORDER BY toplam_gol DESC")
    suspend fun getTopscorer(): List<Topscorer>

    @Query("DELETE FROM topscorer")
    suspend fun deleteTopscorer()

}