package com.nzdinc.hockey.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.nzdinc.hockey.model.leaguetable.Standing
import com.nzdinc.hockey.model.topscorer.Topscorer

@Database(entities = (arrayOf(Topscorer::class)),version = 2)
abstract class hockeyDatabase: RoomDatabase() {

    abstract fun hockeyDao() : hockeyDao

    companion object{
        @Volatile
        private var instance : hockeyDatabase?=null
        private val lock = Any()
        operator fun invoke(context: Context)= instance?: synchronized(lock){
            instance?:makeDatabase(context).also{
                instance = it
            }
        }

        private fun makeDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,hockeyDatabase::class.java, "hockeydatabase")
            .fallbackToDestructiveMigration().build()
    }

}