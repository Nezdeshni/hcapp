package com.nzdinc.hockey.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout

import com.nzdinc.hockey.R
import com.nzdinc.hockey.ui.home.leaguetable.LeagueTableFragment
import com.nzdinc.hockey.ui.home.topscorer.TopScorerFragment
import com.nzdinc.hockey.util.CustomSharedPreferences
///import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        var customPreferences = CustomSharedPreferences(activity?.applicationContext!!)

        val a = customPreferences.getCountryId()
        val tabLayout=view.findViewById<TabLayout>(R.id.tabLayout)
        val viewPager=view.findViewById<ViewPager>(R.id.viewPager)
        Toast.makeText(requireContext(), " id : "+a, Toast.LENGTH_SHORT).show()
        setupUI(tabLayout,viewPager)

    }

    private fun setupUI( tl:TabLayout, vp: ViewPager){
        setupViewpager(vp)
        tl.setupWithViewPager(vp)
    }

    private fun setupViewpager(vp:ViewPager) {
        val adapter = fragmentManager?.let { ViewPagerAdapter(it) }
        adapter?.apply {
            addFragment(LeagueTableFragment(), "League Table")
            addFragment(TopScorerFragment(), "Top Scorers")
        }
        vp.adapter = adapter
//        viewPager.offscreenPageLimit = 2
    }

}
