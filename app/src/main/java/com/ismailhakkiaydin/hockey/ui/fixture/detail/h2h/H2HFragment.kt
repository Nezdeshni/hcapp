package com.nzdinc.hockey.ui.fixture.detail.h2h

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.nzdinc.hockey.R
import com.nzdinc.hockey.base.BaseVMFragment
import com.nzdinc.hockey.model.fixture.Fixture
import com.nzdinc.hockey.model.fixture.HomeTeam
import com.nzdinc.hockey.util.Constant
///import kotlinx.android.synthetic.main.fragment_fixture.*
///import kotlinx.android.synthetic.main.fragment_h2_h.*

class H2HFragment : BaseVMFragment<H2HViewModel>() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_h2_h, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var teamIds = arguments?.getParcelable<Fixture>(Constant.FIXTURE_TEAM_IDS)

        viewModel.getAllH2hItems(teamIds!!.homeTeam.teamİd, teamIds!!.awayTeam.teamİd)
        viewModel.h2hList.observe(viewLifecycleOwner, Observer {
            it?.let {
                val rvDetailH2h=view.findViewById<RecyclerView>(R.id.rvDetailH2h)
                rvDetailH2h.layoutManager = LinearLayoutManager(context)
                rvDetailH2h.adapter = H2hAdapter(it)
            }
        })

        viewModel.loadingH2h.observe(viewLifecycleOwner, Observer {
            it?.let {
                val rvDetailH2h=view.findViewById<RecyclerView>(R.id.rvDetailH2h)
                val progressBarH2h=view.findViewById<ProgressBar>(R.id.progressBarH2h)
                if (it){
                    rvDetailH2h.visibility = View.GONE
                    progressBarH2h.visibility = View.VISIBLE
                }else{
                    rvDetailH2h.visibility = View.VISIBLE
                    progressBarH2h.visibility = View.GONE
                }
            }
        })
    }

    override fun getViewModel(): Class<H2HViewModel> = H2HViewModel::class.java

}
