package com.nzdinc.hockey.ui.fixture.detail

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout

import com.nzdinc.hockey.R
import com.nzdinc.hockey.model.fixture.Fixture
import com.nzdinc.hockey.model.fixture.HomeTeam
import com.nzdinc.hockey.ui.fixture.detail.h2h.H2HFragment
import com.nzdinc.hockey.ui.fixture.detail.statistic.StatisticFragment
import com.nzdinc.hockey.util.Constant
import com.nzdinc.hockey.ui.fixture.FixtureFragment
///import kotlinx.android.synthetic.main.fragment_fixture_detail.*

class FixtureDetailFragment : Fragment() {

    private var bundle = Bundle()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_fixture_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        var result = arguments?.getParcelable<Fixture>(Constant.FIXTURE_TEAM_IDS)
        val viewPagerFixture=view.findViewById<ViewPager>(R.id.viewPagerFixture)
        val tabLayoutFixture=view.findViewById<TabLayout>(R.id.tabLayoutFixture)
        tabLayoutFixture.setupWithViewPager(viewPagerFixture)
        val adapter = fragmentManager?.let { BasePagerAdapter(it) }
        bundle.putParcelable(Constant.FIXTURE_TEAM_IDS, result)
        var h2hFragment = H2HFragment()
        var statisticFragment = StatisticFragment()
        h2hFragment.arguments = bundle
        statisticFragment.arguments = bundle
        adapter?.apply {
            addFragment(h2hFragment, "H2H")
            addFragment(statisticFragment, "STATS")
        }


        viewPagerFixture.adapter = adapter
    }


}
