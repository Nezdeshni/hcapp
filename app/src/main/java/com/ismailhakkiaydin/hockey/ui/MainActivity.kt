package com.nzdinc.hockey.ui

import android.graphics.drawable.Icon
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.nzdinc.hockey.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navController = findNavController(R.id.fragment)
        navController.setGraph(R.navigation.navigation_graph)
        //nv=this.findViewById(/* id = */ R.id.bottomNavigationView)
        //mi.
        //NavigationUI.setupWithNavController(bottomNavigationView, navController)
       // findViewById<Icon>(R.id.settingFragment)
       /* button.setOnClickListener {
            findNavController().navigate(R.id.fragment_profile) // Navigate to ProfileFragment
        }*/
      //  val navHostFragment = supportFragmentManager.findFragmentById(R.id.fragment) as NavHostFragment

    }
}
