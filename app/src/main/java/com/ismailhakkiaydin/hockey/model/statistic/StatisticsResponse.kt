package com.nzdinc.hockey.model.statistic


import com.google.gson.annotations.SerializedName
import com.nzdinc.hockey.model.statistic.Api

data class StatisticsResponse(
    @SerializedName("api")
    var api: Api
)